package at.spenger.xml.bibliothek;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Stack;

import javax.xml.bind.ParseConversionEvent;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.XMLEvent;

import org.supercsv.cellprocessor.ParseBigDecimal;

import scala.annotation.meta.param;

public class StaXParser {

	public List<Buch> read(InputStream in) throws IOException {
		final String ITEM = "item";
		List<Buch> buecher = new ArrayList<>();

		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document
			Buch item = null;
			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {

					switch (ev.asStartElement().getName().getLocalPart()) {
					case ITEM:
						// If we have an item element, we create a new item
						item = new Buch();
						break;
					case "title":
						ev = eventReader.nextEvent();
						item.setTitle(ev.asCharacters().getData());
						continue;
					case "author":
						ev = eventReader.nextEvent();
						item.setAuthor(ev.asCharacters().getData());
						continue;
					case "quantity":
						ev = eventReader.nextEvent();
						item.setQuantity(Integer.parseInt(ev.asCharacters().getData()));
						continue;
					case "price":
						ev = eventReader.nextEvent();
						
						DecimalFormatSymbols symbols = new DecimalFormatSymbols();
						symbols.setGroupingSeparator(',');
						symbols.setDecimalSeparator('.');
						String pattern = "#,##0.0#";
						DecimalFormat decimalFormat = new DecimalFormat(pattern, symbols);
						decimalFormat.setParseBigDecimal(true);
						try
						{
						BigDecimal bigDecimal = (BigDecimal) decimalFormat.parse(ev.asCharacters().getData());
						item.setPrice(bigDecimal);
						}
						catch (Exception e)
						{}
						
						continue;
					}
				}
				if (ev.isEndElement()) {
					if (ev.asEndElement().getName().getLocalPart()
							.equals(ITEM)) {
						buecher.add(item);
					}

				}
			}
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return buecher;
	}

	public void readSimple(InputStream in) throws IOException {
		// First, create a new XMLInputFactory
		XMLInputFactory inputFactory = XMLInputFactory.newInstance();

		Stack<String> stck = new Stack<String>();

		// Setup a new eventReader
		try {
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);

			while (eventReader.hasNext()) {
				XMLEvent ev = eventReader.nextEvent();
				if (ev.isStartElement()) {
					stck.push(ev.asStartElement().getName().getLocalPart());

					@SuppressWarnings("unchecked")
					Iterator<Attribute> iter = ev.asStartElement()
							.getAttributes();
					while (iter.hasNext()) {
						Attribute a = iter.next();
						System.out.println(buildXPathString(
								stck,
								"/@" + a.getName().getLocalPart() + "=\""
										+ a.getValue() + "\""));
					}
				}
				if (ev.isCharacters()) {
					String s = ev.asCharacters().getData();
					if (s.trim().length() > 0)
						System.out.println(buildXPathString(stck, "=\"" + s
								+ "\""));
				}
				if (ev.isEndElement())
					stck.pop();

			}

		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private String buildXPathString(Stack<String> stck, String postfix) {
		StringBuffer sb = new StringBuffer();
		for (String s : stck)
			sb.append("/").append(s);
		sb.append(postfix);
		return sb.toString();
	}

}
