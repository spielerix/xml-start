package at.spenger.xml;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;
import org.springframework.core.io.ClassPathResource;

import at.spenger.xml.bibliothek.Buch;
import at.spenger.xml.bibliothek.StaXParser;

@Import(DefaultConfig.class)
public class Application implements CommandLineRunner {

	public static void main(String[] args) {
		 new SpringApplicationBuilder(Application.class)
		    .showBanner(false)
		    .logStartupInfo(false)
		    .run(args);
	}

	@Override
	public void run(String... arg0) throws Exception {
		listBuecher();
		
		// Heise News
		//XPathReader xPathReader = new XPathReader();
		//xPathReader.readHeiseFeed();
	}
	
	private void listBuecher() throws IOException {
		//Laedt die XML datei aus dem recource Ordner
		InputStream in = new ClassPathResource("buchausborgen.xml")
				.getInputStream();
		
		StaXParser p = new StaXParser();
		
		//Holt sich eine Collection aller Buecher aus dem StaxParser Objekt
		List<Buch> items = p.read(in);

		//und ruft deren toString methode auf um diese auszugeben
		for (Buch item : items) {
			System.out.println(item);
		}
	}
}
